var config = require('config')
const influx = require('influx')

const InfluxDb = new influx.InfluxDB({
    host: config.get('InfluxDb.host'),
    database: config.get('InfluxDb.database'),
    port: config.get('InfluxDb.port')
  })

models = {
  InfluxDb: InfluxDb,
  Measurement : require('./meas.model'),
  Field : require('./field.model'),
  DataPoint : require('./datapoint.model'),
  DataSet : require('./dataset.model')
}
module.exports = models;