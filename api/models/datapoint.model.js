
/**
 * Creates a DataPoint
 * @class
 */
function DataPoint(time, value) {
    this.time = time
    this.value = value
}

module.exports = DataPoint;