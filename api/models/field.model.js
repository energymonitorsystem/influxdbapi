DataPoint = require ('.').DataPoint

/**
 * Creates a Field
 * @class
 */
function Field(name) {
    this.name = name
    this.first = null
    this.last = null
    this.count = 0
}

module.exports = Field;