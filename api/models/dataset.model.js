DataPoint = require ('.').DataPoint

/**
 * Creates a DataSet
 * @class
 */
function DataSet(measurement, field) {
    this.measurement = measurement
    this.field = field
    this.aggregationFunction = "none"
    this.aggregationTime = 0
    this.datapoints = new Array()
}

/**
 * Add point to dataset
 * @param {DataPoint} datapoint
 */
DataSet.prototype.addDataPoint = function(datapoint) {
    //if(datapoint instanceof DataPoint){
        this.datapoints.push(datapoint)
        return true
    /*}else
        return false
    */
};

/**
 * Add a list of points to dataset
 * @param {DataPoint|Array} datapoint
 */
DataSet.prototype.setDataPoints = function(datapointList) {
    //if(datapoint instanceof DataPoint){
        this.datapoints = datapointList
        return true
    /*}else
        return false
    */
};

module.exports = DataSet;