Field = require ('.').Field

/**
 * Creates a Measurement
 * @class
 */
function Measurement(name) {
    this.name = name
    this.fields = new Array()
}

/**
 * Add a field to measurement
 * @param {Field} field
 */
Measurement.prototype.addField = function(field) {
    //if(datapoint instanceof DataPoint){
        this.fields.push(field)
        return true
    /*}else
        return false
    */
};

module.exports = Measurement;