var config = require('config')
var models = require("../models");
var exceptions = require("../exceptions")
var dateparser = require('date-and-time');

var InfluxDb = models.InfluxDb

/**
 * @description Validate pagination parameters
 * 
 * @param {number} limit Limit the records to return
 * @param {number} offset paginates N records
 * 
 * @return {Object} Object with limit and offset values
 * 
 * @throws {BadParameterException}
 */
function validatePaginationParams(limit, offset){
    
    // query limit
    if (limit != undefined && isNaN(limit)) 
        throw new exceptions.BadParameterException("limit parameter must be an integer value")
    var retLimit = limit != undefined && limit < config.get("InfluxDb.queryLimit") ? limit : config.get("InfluxDb.queryLimit")

    // query offset
    if (offset != undefined && isNaN(offset)) 
        throw new exceptions.BadParameterException("offset parameter must be an integer value")

    var retOffset = offset != undefined ? params.offset : 0

    result = {
        limit: parseInt(retLimit, 10),
        offset: parseInt(retOffset, 10)
    };

    logger.debug('Limit query to %d results (param: %d)', result.limit, limit)
    logger.debug('Offset query to %d (param: %d)', result.offset, offset)

    return result
}

/**
 * @description Validate time parameters
 * 
 * @param {number} start UTC startime for query records in format 'YYYYMMDDHHmmss'
 * @param {number} end UTC endtime for query records in format 'YYYYMMDDHHmmss'
 * @param {number} range time range aplied to starttime for query records in seconds. Requires start parameter
 * @param {Field} fldInfo Field information object
 * 
 * @return {Object} Object with start time, end time and range in nanoseconds or NaN
 * 
 * @throws {BadParameterException}
 */
function validateTimeParams(start, end, range, fldInfo){
    
    // startTime parameter
    var nsStartTime = 0
    try{
        var startTime = start == undefined ? fldInfo.first.time : dateparser.parse(start, "YYYYMMDDHHmmss")
        
        if(startTime < fldInfo.first.time) 
            startTime = fldInfo.first.time
        
        nsStartTime = startTime.getTime() * 1000000
    }catch(e){
        throw new exceptions.BadParameterException("Start time must be defined in 'YYYYMMDDHHmmss' format")
    }

    // endTime parameter
    try{
        var endTime = end == undefined ? fldInfo.last.time : dateparser.parse(end, "YYYYMMDDHHmmss")

        if(endTime > fldInfo.last.time) 
            endTime = fldInfo.last.time

        var nsEndTime = endTime.getTime() * 1000000
    }catch(e){
        throw new exceptions.BadParameterException("End time must be defined in 'YYYYMMDDHHmmss' format")
    }

    if(end != undefined && start == undefined)
        throw new exceptions.BadParameterException("End parameter requires a starttime")

    if(nsEndTime < nsStartTime)
        throw new exceptions.BadParameterException("Endtime must be greater than startime")

    // range parameter
    if(range != undefined){
        if (start == undefined)
            throw new exceptions.BadParameterException("Range parameter requires a starttime")
    
        if(end != undefined)
            throw new exceptions.BadParameterException("Range and end parameters are mutually exclusives")

        if (isNaN(range)) 
            throw new exceptions.BadParameterException("range parameter must be an integer value")
    
        var nsRangeTime = range * 1000000000

        nsStartTime = nsRangeTime > 0 ? nsStartTime : nsStartTime + nsRangeTime
        nsEndTime = nsRangeTime > 0 ? nsStartTime + nsRangeTime : nsStartTime
    }

    result = {
        start: nsStartTime,
        end: nsEndTime
    }

    logger.debug("StartTime '%s (%d)' (param: %d)", new Date(result.start / 1000000 ), result.start, start)
    logger.debug("EndTime '%s (%d)' (param: %d)",  new Date(result.end / 1000000 ), result.end, end)
    logger.debug("Range time '%dsegs'", range)

    return result
}

/**
 * @description Validate aggregation parameters
 * 
 * @param {*} aggrFcn Aggregation function
 * @param {*} time Time in seconds as interval to aggregate values
 * @param {*} aggrFcnList List of allowed aggregation functions
 * 
 * @return {Object} Object with aggregation function and time
 * 
 * @throws {BadParameterException}
 */
function validateAggregationParams(aggrFcn, time, aggrFcnList){

    aggrFcnList.forEach((fcn, idx) => {
        aggrFcnList[idx] = fcn.toUpperCase()
    })
    
    // Aggregation function
    var resAggrFcn = ""
    if (aggrFcn != undefined && aggrFcnList.indexOf(aggrFcn.toUpperCase()) > -1) 
        resAggrFcn = aggrFcn.toUpperCase()
    else
        throw new exceptions.BadParameterException("Aggregation function must matches with '" + aggrFcnList + "'")

    if (aggrFcn != undefined && time == undefined) 
        throw new exceptions.BadParameterException("Aggregation function requires an aggregation time")
    
    // Aggregation time
    if (time != undefined && isNaN(time)) 
        throw new exceptions.BadParameterException("Aggregation time must be an integer value")

    if (time != undefined && aggrFcn == undefined) 
        throw new exceptions.BadParameterException("Aggregation time requires an aggregation function")

    return {
        fcn: resAggrFcn,
        time: parseInt(time, 10)
    }
}

/**
 * Get points from a query returned object
 * @param {Object} qResult Object with the query results
 * 
 * @return Points from query result
 */
function doQueryResult(qResult){
    if(qResult[0][0] == undefined)
        reject(new Error("Field does not exist" ))
    
    // Get total points
    var total = qResult[0][0].count
    
    // Get dataset points
    var points = qResult[1]

    var count = 0
    var dsPoints = new Array()
    points.forEach((point) => {
        var dsp = new models.DataPoint(point.time, point.value)
        dsPoints.push(dsp)
        count++
    })

    return {
        total: total,
        count: count,
        points: dsPoints
    }

}

/**
 * @description Get a timeseries from a measurement field
 * 
 * @param {string} meas Measurement
 * @param {Object} fieldInfo Field object with field information
 * @param {Object} params Parameters object from request
 * @param {string} params.start UTC startime for query records in format 'YYYYMMDDHHmmss'
 * @param {string} params.end UTC endtime for query records in format 'YYYYMMDDHHmmss'
 * @param {string} params.range time range aplied to starttime for query records in seconds. Requires start parameter
 * @param {string} params.limit returns the first N records
 * @param {string} params.offset paginates N records 
 * 
 * @return {Promise} List of fields datapoint and extra information
 * 
 * @throws {BadParameterException}
 */
//service.getSerieByMeasAndField = function(meas, fieldInfo, params){
function getSerieByMeasAndField(meas, fieldInfo, params){
    return new Promise(function(resolve, reject){
        
        logger.info("Getting series points -> Measurement: '%s' - Field: '%s'", meas, fieldInfo.name)

        var pagParams = validatePaginationParams(params.limit, params.offset)
        var timeParams = validateTimeParams(params.start, params.end, params.range, fieldInfo)
        
        var queries = new Array()
        queries.push("SELECT COUNT(" + fieldInfo.name + ") FROM " + meas)
        queries.push("SELECT " + fieldInfo.name + " AS value FROM " + meas)
        
        queries.forEach((query, idx) => {
            // where clause
            queries[idx] += " WHERE time >= " + timeParams.start + " AND time <= " + timeParams.end

            // Limit & offeset clause
            if(idx > 0)
                queries[idx] += " LIMIT " + pagParams.limit + " OFFSET " + pagParams.offset
        })

        var resJson = {
            total: 0,
            count: 0,
            limit: pagParams.limit,
            offset: pagParams.offset,
            dataset: {}
        }

        var strQueries = queries.join(';')
        logger.debug("Running queries '%s'", strQueries)
        InfluxDb.query(strQueries)
        .then((result) => {
            logger.debug("Getting '%d' results", result.length)

            var ds = new models.DataSet(meas, fieldInfo.name)
            var res = doQueryResult(result)

            ds.setDataPoints(res.points)

            resJson.total = res.total
            resJson.count = res.count
            resJson.dataset = ds

            resolve(resJson)

        }).catch(err =>{
            reject(err)
        }) 
    })
}

/**
 * @description Get a timeseries from a measurement field
 * 
 * @param {string} meas Measurement
 * @param {Object} fieldInfo Field object with field information
 * @param {string} aggrfcn Aggregation function. Requires aggrtime parameter
 * @param {number} aggrtime Time in seconds as interval to aggregate values. Requires aggrfcn parameter
 * @param {Object} params Parameters object from request
 * @param {string} params.start UTC startime for query records in format 'YYYYMMDDHHmmss'
 * @param {string} params.end UTC endtime for query records in format 'YYYYMMDDHHmmss'
 * @param {string} params.range time range aplied to starttime for query records in seconds. Requires start parameter
 * @param {string} params.limit returns the first N records
 * @param {string} params.offset paginates N records 
 * 
 * @return {Promise} List of fields datapoint and extra information
 * 
 * @throws {BadParameterException}
 */
function getAggregatedSerieByMeasAndField(meas, fieldInfo, aggrFcn, aggrTime, params){
    return new Promise(function(resolve, reject){
        console.log("fld: " + fieldInfo)

        logger.info("Getting aggregated series points -> Measurement: '%s' - Field: '%s' - Function: '%s' - time: '%ds'", meas, fieldInfo.name, aggrFcn, aggrTime)

        var aggrFunctions = ['min', 'max', 'mean', 'difference']

        var pagParams = validatePaginationParams(params.limit, params.offset)
        var timeParams = validateTimeParams(params.start, params.end, params.range, fieldInfo)
        var aggrParams = validateAggregationParams(aggrFcn, aggrTime, aggrFunctions)
        
        var queries = new Array()
        queries.push("SELECT COUNT(" + fieldInfo.name + ") FROM " + meas)
        queries.push("SELECT " + aggrParams.fcn + "(" + fieldInfo.name + ") AS value FROM " + meas)
        
        queries.forEach((query, idx) => {
            // where clause
            queries[idx] += " WHERE time >= " + timeParams.start + " AND time <= " + timeParams.end

            // group by clause
            queries[idx] += " GROUP BY time(" + aggrParams.time + "s)"

            // Limit & offeset clause
            if(idx > 0)
                queries[idx] += " LIMIT " + pagParams.limit + " OFFSET " + pagParams.offset
           // console.log(query)
        })

        var resJson = {
            total: 0,
            count: 0,
            limit: pagParams.limit,
            offset: pagParams.offset,
            dataset: {}
        }

        var strQueries = queries.join(';')
        logger.debug("Running queries '%s'", strQueries)
        InfluxDb.query(strQueries)
        .then((result) => {
            logger.debug("Getting '%d' results", result.length)

            var ds = new models.DataSet(meas, fieldInfo.name)
            var res = doQueryResult(result)

            ds.setDataPoints(res.points)

            resJson.total = res.total
            resJson.count = res.count
            resJson.dataset = ds
            resJson.dataset.aggregationFunction = aggrParams.fcn
            resJson.dataset.aggregationTime = aggrParams.time

            resolve(resJson)

        }).catch(err =>{
            reject(err)
        }) 
    })
}

module.exports = {
    getSerieByMeasAndField: getSerieByMeasAndField,
    getAggregatedSerieByMeasAndField: getAggregatedSerieByMeasAndField
}
