var seriesSvc = require('./series.service');
var measSvc = require('./meas.service');


module.exports.series = {  
	get: seriesSvc.getSerie,
	getSerie: seriesSvc.getSerieByMeasAndField,
	getAggrSerie: seriesSvc.getAggregatedSerieByMeasAndField
}

module.exports.meas = {  
	getFieldData: measSvc.getFieldData,
	getMeasData: measSvc.getMeasData,
	getAllMeasData: measSvc.getAllMeasData
}