var models = require("../models");
var logger = require("winston");
//var dateparser = require('date-and-time');

var InfluxDb = models.InfluxDb

var service = {};

/**
 * Get information about a field
 * @param {string} meas
 * @param {string} fiels
 * 
 * @return {Promise} Field information
 */
service.getFieldData = function(meas, field){

    return new Promise(function(resolve, reject){
        logger.info("Getting field data -> Measurement: '%s' - Field: '%s'", meas, field)

        var fieldObj = new models.Field(field)
        
        queries = "SELECT FIRST(" + field + ") AS value FROM " + meas
        queries += ";SELECT LAST(" + field + ") AS value FROM " + meas
        queries += ";SELECT COUNT(" + field + ") AS value FROM " + meas
        
        logger.log('debug', "Running queries '%s'", queries)
        InfluxDb.query(queries)
        .then((result) => {
            if(result[0][0] == undefined){
                reject(new Error("Field '" + field + "' does not exist for measurement '" + meas + "'"))
            }
            
            fieldObj.first = new models.DataPoint(new Date(result[0][0].time.getNanoTime()/1000000), result[0][0].value)
            fieldObj.last = new models.DataPoint(new Date(result[1][0].time.getNanoTime()/1000000), result[1][0].value)
            fieldObj.count = result[2][0].value
            resolve(fieldObj)

        }).catch(err =>{
            reject(err)
        }) 
    })
    
}

/**
 * Get information about a measurement and its fields
 * @param {string} meas
 * 
 * @return {Promise} Measurement information
 */
service.getMeasData = function(meas){

    return new Promise(function(resolve, reject){
        var measObj = new models.Measurement(meas)

        logger.info("Getting measurement data -> Measurement: '%s'", meas)
        
        var query = "SHOW FIELD KEYS FROM " + meas
        
        logger.log('debug', "Running query '%s'", query)

        InfluxDb.query(query)
        .then((result) => {
        
            if(result.groupRows[0] == undefined){
                reject(new Error("Measurement '" + meas + "' does not exists"))
            }
            
            var promisesList = result.groupRows[0].rows.map(function(field){
                return new Promise(function(pass, fail){
                    service.getFieldData(meas, field.fieldKey)
                    .then(field =>{
                        measObj.addField(field)
                        pass()
                    })
                    .catch(fail)
                })
            })

            Promise.all(promisesList)
            .then(function() {
                resolve(measObj)
            })
            .catch(function(err) {
                reject(err)
            });
        }).catch(err =>{
            reject(err)
        }) 
    })
}

/**
 * Get information about all measurements and their fields
 * 
 * @return {Promise} Measurement information list
 */

service.getAllMeasData = function(){
    return new Promise(function(resolve, reject){
        
        logger.info("Getting all measurements data")
        var result = new Array()
        
        var query = "SHOW MEASUREMENTS"
        logger.log('debug', "Running query '%s'", query)
        
        InfluxDb.query(query)
        .then((measList) => {
            var promisesList = measList.groupRows[0].rows.map(function(meas){
                return new Promise(function(pass, fail){
                    service.getMeasData(meas.name)
                    .then(measObj =>{
                        result.push(measObj)
                        pass()
                    })
                    .catch(fail)
                })
            })

            Promise.all(promisesList)
            .then(function() {
                resolve(result)
            })
            .catch(function(err) {
                reject(err)
            });
        }).catch(err =>{
            reject(err)
        }) 
    })
}

module.exports = service;