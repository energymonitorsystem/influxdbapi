var logger = require('winston');
//var dateFormat = require('dateformat');
var dateparser = require('date-and-time');

module.exports.configure=function(config){
	logger.remove(logger.transports.Console);
	
	if (config.has('console')) {
		logger.add(logger.transports.Console,{ 
			level: config.get('console.level'),
			colorize: false,
			json:false,
			handleExceptions: true,
			humanReadableUnhandledException: true,
			formatter: function (args) {
				//return dateFormat(new Date(), config.get('console.dateFormat')) + ' - [' + logger.config.colorize(args.level) + '] - ' + args.message;
				//return dateparser.format(new Date(), config.get('console.dateFormat')) + ' - [' + logger.config.colorize(args.level) + '] - ' + logger.config.colorize(args.level, args.message);
				return dateparser.format(new Date(), config.get('console.dateFormat')) + logger.config.colorize(args.level, ' - [' + args.level + '] - ' + args.message);
			}
		});
	}
	if (config.has('file')) {
		logger.add(logger.transports.File,{ 
			level: config.get('file.level'),
			filename: process.cwd() + config.get('file.filename'),
			maxSize: config.get('file.maxSize'),
			maxFiles: config.get('file.maxFiles'),
			tailable: true,
			json:false,
			handleExceptions: true,
			humanReadableUnhandledException: true,
			formatter: function (args) {
				return dateparser.format(new Date(), config.get('file.dateFormat')) + ' - [' + args.level + '] - ' + args.message;
			}
		});
	}
}