const router = require('express').Router();
var config = require('config')
var services = require('../services');
var exceptions = require("../exceptions")

var seriesService = services.series;
var measService = services.meas;

var routerName = 'series'

router.route('/:meas/:field').get(function(req, res){
    logger.info("===============================")
    
    measService.getFieldData(req.params.meas, req.params.field)
    .then(fieldInfo =>{
        logger.info("===============================")
        
        seriesService.getSerie(req.params.meas, fieldInfo, req.query)
        .then(result =>{
            logger.info("===============================")
            res.status(200).json({error: false, serie: result})
        }).catch(err => {
            logger.error(err.stack)
            logger.info("===============================")
            
            if(err instanceof exceptions.BadParameterException)
                res.status(400).json({error: true, message: err.message})
            else
                res.status(500).json({error: true, message: err.message})
        })
    }).catch(function(err){
        logger.error(err.stack)
        logger.info("===============================")
        res.status(500).json({error: true, message: err.message})
    })

    /*********** */
    /*logger.info("===============================")
    
    
    
    seriesService.getSerie(req.params.meas, req.params.field, req.query)
    .then(result =>{
      logger.info("===============================")
      res.status(200).json({error: false, serie: result})
    }).catch(function(err){
      logger.error(err.stack)
      logger.info("===============================")
      if(err instanceof exceptions.BadParameterException)
        res.status(400).json({error: true, message: err.message})
      else
        res.status(500).json({error: true, message: err.message})
      
    })*/
})


router.route('/:meas/:field/:aggrFcn/:aggrTime').get(function(req, res){
    logger.info("===============================")
  
    measService.getFieldData(req.params.meas, req.params.field)
    .then(fieldInfo =>{
        logger.info("===============================")
        
        seriesService.getAggrSerie(req.params.meas, fieldInfo, req.params.aggrFcn, req.params.aggrTime, req.query)
        .then(result =>{
            logger.info("===============================")
            res.status(200).json({error: false, serie: result})
        }).catch(err => {
            logger.error(err.stack)
            logger.info("===============================")
            
            if(err instanceof exceptions.BadParameterException)
                res.status(400).json({error: true, message: err.message})
            else
                res.status(500).json({error: true, message: err.message})
        })
    }).catch(function(err){
        logger.error(err.stack)
        logger.info("===============================")
        res.status(500).json({error: true, message: err.message})
    })
})

module.exports = {
  name: routerName,
  baseUrl: '/' + config.get('root_endpoint') + '/' + routerName,
  router: router
}