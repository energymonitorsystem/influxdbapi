logger = require('winston')
fs = require('fs')

var routers = new Array()
fs.readdirSync(__dirname)
.filter(function(file) {
    return (file.indexOf(".router.js") > 0)
}).forEach(function(file) {
    logger.info("Import router from '%s'", file)

    var router = require("./" + file)
    routers[router.name] = router

    // Getting list of routers paths
    var routes = router.router.stack
    for (var key in routes) {
        if (routes.hasOwnProperty(key) && routes[key].route)
            logger.info("[" +  routes[key].route.stack[0].method.toUpperCase() + "]\t" + router.baseUrl + routes[key].route.path)
    }
});

module.exports = routers
	