const router = require('express').Router();
var config = require('config')
var logger = require('winston')
var services = require('../services');

var measService = services.meas;

var routerName = 'meas'

router.route('/').get(function(req, res){
  logger.info("===============================")
  measService.getAllMeasData()
  .then(measList =>{
    logger.info("===============================")
    res.status(200).json({error: false, measurements: measList})
  }).catch(function(err){
    logger.error(err.stack)
    logger.info("===============================")
    res.status(500).json({error: true, message: err.message})
  })
})

router.route('/:meas').get(function(req, res){
  logger.info("===============================")
  measService.getMeasData(req.params.meas)
  .then(meas =>{
    logger.info("===============================")
    res.status(200).json({error: false, measurement: meas})
  }).catch(function(err){
    logger.error(err.stack)
    logger.info("===============================")
    res.status(500).json({error: true, message: err.message})
  })
})

router.route('/:meas/:field').get(function(req, res){
  logger.info("===============================")
  measService.getFieldData(req.params.meas, req.params.field)
  .then(field =>{
    logger.info("===============================")
    res.status(200).json({error: false, measurement: req.params.meas, field: field})
  }).catch(function(err){
    logger.error(err.stack)
    logger.info("===============================")
    res.status(500).json({error: true, message: err.message})
  })
})

//module.exports = router;
module.exports = {
  name: routerName,
  baseUrl: '/' + config.get('root_endpoint') + '/' + routerName,
  router: router
}