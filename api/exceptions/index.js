
exceptions = {
  BadParameterException : require('./badparameters.exception')
}

module.exports = exceptions;