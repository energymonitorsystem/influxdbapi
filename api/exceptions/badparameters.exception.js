function BadParameterException(message) {
    this.message = message;
    // Use V8's native method if available, otherwise fallback
    if ("captureStackTrace" in Error)
        Error.captureStackTrace(this, BadParameterException);
    else
        this.stack = (new Error()).stack;
}

BadParameterException.prototype = Object.create(Error.prototype);
BadParameterException.prototype.name = "BadParameterException";
BadParameterException.prototype.constructor = BadParameterException;

module.exports = BadParameterException;