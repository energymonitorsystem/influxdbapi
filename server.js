var config = require('config')
var express = require('express')
var bodyParser  = require("body-parser")
var methodOverride = require("method-override")
var logger = require('winston')
var utils = require('./api/utils')

var app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(methodOverride())

// Configure Logger
utils.logger.configure(config.get('logger'))

//var root = '/' + config.get('root_endpoint') 
var routers = require('./api/routes');

// Register routes
for (var key in routers) {
    var baseUrl = routers[key].baseUrl
    var router = routers[key].router

    app.use(baseUrl, router)
}

app.listen(config.get('port') , function() {  
    logger.info("Influx API server running on port '%d' [MODE: %s]", config.get("port"), config.util.getEnv('NODE_ENV'))
})

// For testing
module.exports = app
