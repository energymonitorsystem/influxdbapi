FROM node:8.8.1

WORKDIR /usr/src/influxDbService
#VOLUME /usr/src/influxDbService

# Expose ports
EXPOSE 3000

ENV NODE_ENV production

RUN npm install -g pm2
RUN mkdir logs

# Install dependencies
ADD package.json ./
RUN npm install --production

# COPY APP
ADD config ./config
ADD api ./api
ADD server.js ./

CMD ["pm2", "start", "server.js", "--no-daemon"]