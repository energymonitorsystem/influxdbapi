# **influxDbApi**
REST api to get data from influx mesurements
## **Configuration**
All configurations are stored in the configuration files located under 'config' folder. Every file stored the configuration depends on the environtment where the aplication runs.
```json
{
  "port": 3000,
  "root_endpoint": "v1",
  "InfluxDb": {
    "host": "hostname or host IP",
    "port": 8086,
    "database": "emon",
    "queryLimit": 5000
  }
}
```
**port:** TCP port where the API server is listening

**root_endpoint:** Root endpoint for services endpoints

**InfluxDb:** Influx Database connection properties
- **host:** Host where the database runs
- **port:** Port to database access
- **database:** Name of the database
- **queryLimit:** Maximum records returned by queries
## **Measurements endpoints**
Services to get information about measurements and fields

**/v1/meas** Get information for all measurements stored in influx database and their fields
```
host:port/v1/meas/
```
```json
{
    "error": false,
    "measurements": [
        {
            "name": "volts",
            "fields": [
                {
                    "name": "total",
                    "first": {
                        "time": "2017-08-11T18:18:10.000Z",
                        "value": 241.380004883
                    },
                    "last": {
                        "time": "2017-11-06T18:52:20.000Z",
                        "value": 241.300003052
                    },
                    "count": 708533
                }
            ]
        },
        {
            "name": "power",
            "fields": [
                {
                    "name": "total",
                    "first": {
                        "time": "2017-08-11T18:15:50.000Z",
                        "value": 440
                    },
                    "last": {
                        "time": "2017-11-06T18:52:20.000Z",
                        "value": 373
                    },
                    "count": 708538
                },
                ...
                ...
            ]
        },
        ...
        ...
    ]
}
```
**v1/meas/:meas** Get information for a specific measurement and its fields
```
host:port/v1/meas/power
```
```json
{
    "error": false,
    "measurement": {
        "name": "power",
        "fields": [
            {
                "name": "total",
                "first": {
                    "time": "2017-08-11T18:15:50.000Z",
                    "value": 440
                },
                "last": {
                    "time": "2017-11-06T18:48:20.000Z",
                    "value": 370
                },
                "count": 708514
            },
            ...
            ...
        ]
    }
}
```
**v1/meas/:meas/:field** Get information specific measurement field 
```
host:port/v1/meas/power/total
```
```json
{
    "error": false,
    "measurement": "power",
    "field": {
        "name": "total",
        "first": {
            "time": "2017-08-11T18:15:50.000Z",
            "value": 440
        },
        "last": {
            "time": "2017-11-06T18:50:20.000Z",
            "value": 372
        },
        "count": 708526
    }
}
```
## **Series endpoints**
Services to get datapoints of measurement fields

### **v1/series/:meas/:field** 
Get datapoints for a specific measurement field 

#### ***Parameters:***
- **meas:** Mesurement name
- **field:** Field name
#### ***Query parameters:***
- **limit:** Returns the first *N* records
- **offset:** Paginates *N* records 
- **start:** UTC start time for query records in format 'YYYYMMDDHHmmss'
- **end:** UTC end time for query records in format 'YYYYMMDDHHmmss'. Requires *'start'* parameter
- **range:** Time (seconds) range applied to start time to query records. A negative value stablish a range in the past. Requires *'start'* parameter
```
host:port/v1/series/power/total
```
```json
{
    "error": false,
    "serie": {
        "total": 746326,
        "count": 5000,
        "limit": 5000,
        "offset": 0,
        "dataset": {
            "measurement": "power",
            "field": "total",
            "aggregationFunction": "none",
            "aggregationTime": 0,
            "datapoints": [
                {
                    "time": "2017-08-11T18:15:50.000Z",
                    "value": 440
                },
                {
                    "time": "2017-08-11T18:16:00.000Z",
                    "value": 449
                },
                ...
                ...
            ]
        }
    }
}
```

### **v1/series/:meas/:field/:aggrFcn/:aggrTime** 
Get datapoints for a specific measurement field 

#### ***Parameters:***
- **meas:** Mesurement name
- **field:** Field name
- **aggrFcn:** Aggregation function
- **aggrTime:** Time (seconds) to perform the aggregation
#### ***Query parameters:***
- **limit:** Returns the first *N* records
- **offset:** Paginates *N* records 
- **start:** UTC start time for query records in format 'YYYYMMDDHHmmss'
- **end:** UTC end time for query records in format 'YYYYMMDDHHmmss'. Requires *'start'* parameter
- **range:** Time (seconds) range applied to start time to query records. A negative value stablish a range in the past. Requires *'start'* parameter
```
host:port/v1/series/power/total/meas/3600
```
```json
{
    "error": false,
    "serie": {
        "total": 264,
        "count": 2350,
        "limit": 5000,
        "offset": 0,
        "dataset": {
            "measurement": "power",
            "field": "total",
            "aggregationFunction": "MEAN",
            "aggregationTime": 3600,
            "datapoints": [
                {
                    "time": "2017-08-11T18:00:00.000Z",
                    "value": 303.42424242424244
                },
                {
                    "time": "2017-08-11T19:00:00.000Z",
                    "value": 197.68994413407822
                },
                {
                    "time": "2017-08-11T20:00:00.000Z",
                    "value": 202.16338028169014
                },
                ...
                ...
                
            ]
        }
    }
}
```