//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let config = require('config')
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

let logger = require('winston')

let server = require('../server');

let models = require('../api/models');

let InfluxDb = models.InfluxDb

chai.use(chaiHttp);


let startDate = new Date("2017-11-01T00:00:00.000Z")
let nDataDays = 2
let meas = 'power'
let field = 'total'

//Our parent block
before( function(done){ 
    var dbName = config.get('InfluxDb.database')
    
    // Drop database created in previous tests
    InfluxDb.dropDatabase(dbName)
    .then(() => {
        // Create the database
        InfluxDb.createDatabase(dbName)
        .then(() => {
            // Do mock points
            var startTime = startDate
            var endTime = new Date(startTime.getTime() + (nDataDays*24*60*60*1000))
            var value = 0

            var points = new Array()
            while(startTime < endTime){
                var fields = {}
                fields[field] = value++
                
                points.push({measurement: meas, fields: fields, timestamp: startTime})
                startTime = new Date(startTime.getTime() + (10*1000))
            }

            // Write mock points
            InfluxDb.writePoints(points)
            .then(() => {
                done()
            })
        })
    })
});
/*
after( function(){ 
    console.log('after'); 

    var dbName = config.get('InfluxDb.database')
    InfluxDb.getDatabaseNames()
    .then(names => {
        if (names.includes(dbName)) {
        return InfluxDb.dropDatabase(dbName);
        }

        done()
    })
});
*/
describe('Series', () => {
   
    //beforeEach( function(){ console.log('beforeEach'); });
    //afterEach( function(){ console.log('afterEach'); });
    //it('test 1', function(){ console.log('1'); });
    //it('test 2', function(){ console.log('2'); });

    describe("/GET series", () => {
        it("it should GET a field serie", (done) => {
            chai.request(server)
            .get('/' + config.get('root_endpoint') + '/series/power/total')
            .end((err, res) => {
                res.should.have.status(200);
                var body = res.body

                body.error.should.equal(false)
                
                var serie = body.serie
                serie.total.should.be.a('number')
                serie.total.should.equal(nDataDays*24*60*60/10)

                serie.count.should.be.a('number')
                serie.count.should.equal(config.get("InfluxDb.queryLimit"))

                serie.limit.should.be.a('number')
                serie.limit.should.equal(config.get("InfluxDb.queryLimit"))

                serie.offset.should.be.a('number')
                serie.offset.should.equal(0)

                var dataset = serie.dataset
                dataset.measurement.should.be.a('string')
                dataset.measurement.should.equal(meas)

                dataset.field.should.be.a('string')
                dataset.field.should.equal(field)

                var datapoints = dataset.datapoints
                datapoints.should.be.a('array')
                datapoints.should.be.a('array').with.lengthOf(config.get("InfluxDb.queryLimit"))

                datapoints.forEach((dp, idx) => {
                    var time = new Date(dp.time)
                    var expectedTime = new Date(startDate.getTime() + (idx*10*1000))
                    time.should.eql(expectedTime)

                    dp.value.should.be.a('number')
                    dp.value.should.equal(idx)
                })

                done();
            });
        });

        var start='23ds'
        it("it should ERROR if start doesn't a integer (" + start + ")", (done) => {
            chai.request(server)
            .get('/' + config.get('root_endpoint') + '/series/power/total?start=' + start)
            .end((err, res) => {
                res.should.have.status(400);
                var body = res.body

                body.error.should.equal(true)
                body.message.should.a('string')
                
                done();
            });
        });

        var start='20170111'
        it("it should ERROR if start has bad format (" + start + ")", (done) => {
            chai.request(server)
            .get('/' + config.get('root_endpoint') + '/series/power/total?start=' + start)
            .end((err, res) => {
                res.should.have.status(400);
                var body = res.body

                body.error.should.equal(true)
                body.message.should.a('string')
                    
                done();
            });
        });

        var start='20171311000000'
        it("it should ERROR if start is not a valid date (" + start + ")", (done) => {
            chai.request(server)
            .get('/' + config.get('root_endpoint') + '/series/power/total?start=' + start)
            .end((err, res) => {
                res.should.have.status(400);
                var body = res.body

                body.error.should.equal(true)
                body.message.should.a('string')
                    
                done();
            });
        });
    });

})